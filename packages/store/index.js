module.exports = {
  Client: require('./lib/client'),
  DTO: require('./lib/dto'),
  Store: require('./lib/store')
}