module.exports = {
  Audio: require('./lib/audio'),
  Message: require('./lib/message'),
  Reply: require('./lib/reply'),
  Transcription: require('./lib/transcription').Transcription,
  TranscriptionResult: require('./lib/transcription').TranscriptionResult,
}