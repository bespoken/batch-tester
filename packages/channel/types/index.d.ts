export const Audio: typeof import("./lib/audio");
export const Message: typeof import("./lib/message");
export const Reply: typeof import("./lib/reply");
export const Transcription: typeof import("./lib/transcription").Transcription;
export const TranscriptionResult: typeof import("./lib/transcription").TranscriptionResult;
